# Trip Calculator Kata

1. Complete the `calculate()` method in the `TripCalculator` class
2. The method takes an Array of Trip objects, each which contains:
    * `Date/Time start`  : date and time the trip started
    * `Date/Time end` : date and time when the trip ended
    * `double distance` : the total distance travelled
    * `double milesPerGallon` : the MPG of the vehicle being driven
    * `double costPerGallon` : the cost of a gallon of fuel at the time of the trip
3. The method should calculate the following for the total of all trips in the array:
    * total distance travelled
    * total time travelling (in hours)
    * gallons of fuel used
    * total cost of the fuel used
    * average miles per hour
4. The method returns an array of type double[5], where each of the calculated values should be at a given index:
    * [0] - Total Distance
    * [1] - Total time (in hours)
    * [2] - Average mph
    * [3] - Fuel used
    * [4] - Total Cost
5. If the input array is null or empty, then the returned double[5] array should contain all zeros.
6. Test cases are provided.  Values will be asserted to 2 decimal places.

Hint: Total time should be in hours, including parts of the hour, and not a double formatted as h.m

## Examples

### 1)

Input: `null`

Output:  `double[5] { 0, 0, 0, 0, 0}`

### 2)

Input: `Trip1: Start: August 20, 2019 at 8:00 ; End: August 20, 2019 at 10:00 ; 120 miles ; 24 mpg ;  $4.25 per gallon`

`Trip1: Start: July 4, 2019 at 12:30 ; End: July 4, 2019 at 8:00 ; 100 miles ; 22 mpg ;  $3.70 per gallon`

Output:  `double[5] { 220, 9.5, 41.57, 9.54, 56.56 }`
