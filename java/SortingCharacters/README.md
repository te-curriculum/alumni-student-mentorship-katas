# Sorting Characters Kata

1. Complete the `sort()` method in the `CharacterSorter`
2. The program should return all the characters in the text in sorted alphabetical order.  
3. The program should map all characters to lowercase, and ignore numbers, punctuation, and spaces.
4. The input string may be any length, empty, or null.
5. Test cases are provided

## Examples

### 1)

Input: `null`

Output:  `""`

### 2)

Input: `CaB`

Output:  `abc`

### 3)

Input: `ABc!!`

Output:  `abc`

### 4)

Input:  
`When not studying nuclear physics, Bambi likes to play
beach volleyball.`

Output:  `aaaaabbbbcccdeeeeeghhhiiiiklllllllmnnnnooopprsssstttuuvwyyyy`
