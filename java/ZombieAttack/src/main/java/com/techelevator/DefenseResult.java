package com.techelevator;

public class DefenseResult {

	private boolean successful;
	private int bulletsRemaining;
	private int zombiesRemaining;
	private double metersRemaining;
	
	
	public DefenseResult() {
		
	}
	
	public DefenseResult(boolean successful, int zombiesRemaining, int bulletsRemaining, double metersRemaining) {
		super();
		this.successful = successful;
		this.bulletsRemaining = bulletsRemaining;
		this.zombiesRemaining = zombiesRemaining;
		this.metersRemaining = metersRemaining;
	}
	
	public boolean wasSuccessful() {
		return successful;
	}
	public void setSuccessful(boolean successfull) {
		this.successful = successfull;
	}
	public int getBulletsRemaining() {
		return bulletsRemaining;
	}
	public void setBulletsRemaining(int bulletsRemaining) {
		this.bulletsRemaining = bulletsRemaining;
	}
	public int getZombiesRemaining() {
		return zombiesRemaining;
	}
	public void setZombiesRemaining(int zombiesRemaining) {
		this.zombiesRemaining = zombiesRemaining;
	}
	public double getMetersRemaining() {
		return metersRemaining;
	}
	public void setMetersRemaining(double metersRemaining) {
		this.metersRemaining = metersRemaining;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + bulletsRemaining;
		long temp;
		temp = Double.doubleToLongBits(metersRemaining);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + (successful ? 1231 : 1237);
		result = prime * result + zombiesRemaining;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DefenseResult other = (DefenseResult) obj;
		if (bulletsRemaining != other.bulletsRemaining)
			return false;
		if (Double.doubleToLongBits(metersRemaining) != Double.doubleToLongBits(other.metersRemaining))
			return false;
		if (successful != other.successful)
			return false;
		if (zombiesRemaining != other.zombiesRemaining)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "DefenseResults [successful=" + successful + ", bulletsRemaining=" + bulletsRemaining + ", zombiesRemaining="
				+ zombiesRemaining + ", metersRemaining=" + metersRemaining + "]";
	}


	
	
}
