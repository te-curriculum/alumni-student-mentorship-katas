﻿using System;
using System.Text;

namespace ZombieAttackKata
{
    public class DefenseResult
    {
        public Boolean WasSucessful { get; set; }
        public int BulletsRemaining { get; set; }
        public int ZombiesRemaining { get; set; }
        public double MetersRemaining { get; set; }

        public DefenseResult()
        {

        }

        public DefenseResult( Boolean wasSuccessful, int bulletsRemaining, int zombiesRemaining, double metersRemaining)
        {
            this.WasSucessful = wasSuccessful;
            this.BulletsRemaining = bulletsRemaining;
            this.ZombiesRemaining = zombiesRemaining;
            this.MetersRemaining = metersRemaining;
        }

        public override bool Equals(object obj)
        {
            if (obj == null || !this.GetType().Equals(obj.GetType()) )
            {
                return false;
            }
            DefenseResult other = (DefenseResult)obj;
            if (this.WasSucessful != other.WasSucessful)
            {
                return false;
            } 
            if (this.ZombiesRemaining != other.ZombiesRemaining)
            {
                return false;
            }
            if (this.BulletsRemaining != other.BulletsRemaining)
            {
                return false;
            }
            if (this.MetersRemaining != other.MetersRemaining)
            {
                return false;
            }
            return true;
        }

        public override int GetHashCode()
        {
            int hashCode = unchecked(this.WasSucessful.GetHashCode() * 17 + 
                        this.BulletsRemaining.GetHashCode() * 17 + 
                        this.MetersRemaining.GetHashCode() * 17 + 
                        this.ZombiesRemaining.GetHashCode() * 17);
            return hashCode;
        }

        public override string ToString()
        {
            return new StringBuilder().Append("[ WasSuccessful=").Append(this.WasSucessful)
                .Append(" ZombiesRemaining=").Append(this.ZombiesRemaining)
                .Append(" BulletsRemaining=").Append(this.BulletsRemaining)
                .Append(" MetersRamaining=").Append(this.MetersRemaining).Append(" ]").ToString();
        }
    }
}
