# Zombie Attack Kata

Create an application to calculate if you can successfully defend a fixed position from a zombie attack.

1. Complete the `defend()` method in the `ZombieAttack` class
2. The `defend()` method takes 3 arguments:
    * `int numberOfZombies` : the number of zombies attacking
    * `int numberOfBullets` : the number of bullets you have to defend the position
    * `double meters` : how far the zombies start away from your position
3. Results will be returned as a `DefenseResult` object, which contains
    * `boolean wasSuccessful` : will the defense be successful
    * `int zombiesRemaining` : the number of zombies that will be remain
    * `int bulletsRemaining` : the number of bullets that will be remain
    * `double metersRemaining` : the meters that will remain
4. Test cases have been provided

## Rules

1. The zombies start at the range defined by `meters`, and each tick the zombies move `0.5` meters.
2. You are an expert shot, so for each tick, if there are bullets remaining one zombie should be removed and one bullet.  
3. If there are no zombies remaining before the meters remaining reach 0, then you will be successful.  If there are zombies remaining when the meters reach 0, then you will not be successful.
4. A `tick` is defined as any set amount of time (hint: 1 iteration of a loop)
5. The order of each turn should be:
    * Remove zombie/bullet if enough remains
    * Zombies move
    * Check if an end condition has been reached
6. Return the results when either the number of zombies or meters remaining reach 0.
7. You may not change the `defend()` method signature, `DefenseResult` object, or test cases.

## Examples:

### 1)

Input: `zombies.defend: zombies=100, bullets=100, meters=50`

Output:  `DefenseResult: success=true, zombiesRemaining=0, bulletsRemaining=0, metersRemaining=0.5`

### 2)

Input: `zombies.defend: zombies=50, bullets=200, meters=1000`

Output:  `DefenseResult: success=true, zombiesRemaining=0, bulletsRemaining=150, metersRemaining=975.5`

### 3)

Input: `zombies.defend: zombies=1 bullets=1, meters=0`

Output:  `DefenseResult: success=true, zombiesRemaining=0, bulletsRemaining=0, metersRemaining=0`

### 4)

Input: `zombies.defend: zombies=100, bullets=90, meters=50`

Output:  `DefenseResult: success=false, zombiesRemaining=10, bulletsRemaining=0, metersRemaining=0`

### 5)

Input: `zombies.defend: zombies=2 bullets=2, meters=0.5`

Output:  `DefenseResult: success=false, zombiesRemaining=1, bulletsRemaining=1, metersRemaining=0`
