﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Collections;


namespace TripCalculatorKata
{
    [TestClass]
    public class TripCalculatorTest
    {
        private enum Month
        {
            JANUARY = 1,
            FEBRUARY,
            MARCH,
            APRIL,
            MAY,
            JUNE,
            JULY,
            AUGUST,
            SEPTEMBER,
            OCTOBER,
            NOVEMBER,
            DECEMBER
        }

        private TripCalculator calculator = new TripCalculator();

        [TestMethod]
        public void InvalidInput()
        {
            AssertArraysEquals(new double[5], calculator.calculate(null), "Incorrect results When null");
            AssertArraysEquals(new double[5], calculator.calculate(new Trip[0]), "Incorrect results When array empty");
        }

        [TestMethod]
        public void verify_distance()
        {
            double[] results = calculator.calculate(twoTrips);
            Assert.AreEqual(220, results[0], 2, "Two trips distance incorrect");

            results = calculator.calculate(fourTrips);
            Assert.AreEqual(5901, results[0], 2, "Four trips distance incorrect");

            results = calculator.calculate(oneTrip);
            Assert.AreEqual(120, results[0], 2, "One trip distance incorrect");
        }

        [TestMethod]
        public void verify_time()
        {
            double[] results = calculator.calculate(twoTrips);
            Assert.AreEqual(9.5, results[1], 2, "Two trips time in hours incorrect");

            results = calculator.calculate(fourTrips);
            Assert.AreEqual(422.7, results[1], 2, "Four trips time in hours incorrect");

            results = calculator.calculate(oneTrip);
            Assert.AreEqual(2, results[1], 2, "One trip time in hours incorrect");
        }

        [TestMethod]
        public void verify_mph()
        {
            double[] results = calculator.calculate(twoTrips);
            Assert.AreEqual(41.57, results[2], 2, "Two trips miles Per Hour incorrect");

            results = calculator.calculate(fourTrips);
            Assert.AreEqual(13.96, results[2], 2, "Four trips miles Per Hour incorrect");

            results = calculator.calculate(oneTrip);
            Assert.AreEqual(60, results[2], 2, "One trip miles Per Hour incorrect");
        }

        [TestMethod]
        public void verify_fuel_needed()
        {
            double[] results = calculator.calculate(twoTrips);
            Assert.AreEqual(9.54, results[3], 2, "Two trips gallons of gas needed incorrect");

            results = calculator.calculate(fourTrips);
            Assert.AreEqual(221.14, results[3], 2, "Four trips gallons of gas needed incorrect");

            results = calculator.calculate(oneTrip);
            Assert.AreEqual(5, results[3], 2, "One trip gallons of gas needed incorrect");
        }

        [TestMethod]
        public void verify_total_cost()
        {
            double[] results = calculator.calculate(twoTrips);
            Assert.AreEqual(56.56, results[4], 2, "Two trips total cost incorrect");

            results = calculator.calculate(fourTrips);
            Assert.AreEqual(1668.04, results[4], 2, "Four trips total cost incorrect");

            results = calculator.calculate(oneTrip);
            Assert.AreEqual(21.25, results[4], 2, "One trip total cost incorrect");
        }


        private void AssertArraysEquals(double[] one, double[] two, String message)
        {
            Assert.AreEqual(one.Length, two.Length, message);
            for (int i = 0; i < one.Length; i++)
            {
                Assert.AreEqual(one[i], two[i], message);
            }
        }

        private readonly static Trip[] oneTrip = new Trip[] {
            new Trip( new DateTime(2019, (int) Month.AUGUST, 20, 8, 0, 0) ,
                    new DateTime(2019, (int)Month.AUGUST, 20, 10, 0, 0),
                    120, 24, 4.25
                    )
            };

        private readonly static Trip[] twoTrips = new Trip[] {
            new Trip( new DateTime(2019, (int) Month.AUGUST, 20, 8, 0, 0) ,
                    new DateTime(2019, (int) Month.AUGUST, 20, 10, 0, 0),
                    120, 24, 4.25
                    ),
            new Trip( new DateTime(2019, (int) Month.JULY, 4, 12, 30, 0) ,
                    new DateTime(2019, (int) Month.JULY, 4, 20, 0, 0),
                    100, 22, 3.70
                    )
            };

        private readonly static Trip[] fourTrips = new Trip[] {
            new Trip( new DateTime(2019,(int)  Month.JUNE, 30, 6, 25, 0) ,
                    new DateTime(2019, (int) Month.JULY, 5, 8, 10, 0),
                    1800, 30, 3.85
                    ),
            new Trip( new DateTime(2019, (int) Month.DECEMBER, 31, 16, 15, 0) ,
                    new DateTime(2020, (int) Month.JANUARY, 1, 8, 10, 0),
                    500, 18, 5.78
                    ),
            new Trip( new DateTime(2019, (int) Month.FEBRUARY, 28, 8, 15, 0) ,
                    new DateTime(2019, (int) Month.FEBRUARY, 28, 8, 35, 0),
                    1, 27, 2.78
                    ),
            new Trip( new DateTime(2019, (int) Month.MARCH, 8, 18, 28, 0) ,
                    new DateTime(2019, (int) Month.MARCH, 20, 15, 10, 0),
                    3600, 27, 3.10
                    )
            };

    }
}
